<?php

function foundation_extension_admin_page() {
  dpm(foundation_extension_files('all','all'));
}

function foundation_extension_admin_form() {
  $module_path = drupal_get_path('module', 'foundation_extension');
  if (!isset($form['foundation_extension'])) {
    $form['foundation_extension'] = array(
      '#type' => 'vertical_tabs',
      '#weight' => -10
    );
    $jquery_version = variable_get('jquery_update_jquery_version', '1.7');

    if (!module_exists('jquery_update') || !version_compare($jquery_version, '1.7', '>=')) {
      drupal_set_message(t('!module was not found, or your version of jQuery does not meet the minimum version requirement. Zurb Foundation requires jQuery 1.7 or higher. Please install !module, or Zurb Foundation plugins may not work correctly.',
        array(
          '!module' => l(t('jQuery Update'), 'https://drupal.org/project/jquery_update', array('external' => TRUE, 'attributes' => array('target' => '_blank'))),
        )
      ), 'warning', FALSE);
    }
    $form['foundation_extension']['#attached']['css'][] = $module_path . '/css/foundation_extension.admin.css';
    /*
     * General Settings.
     */
    $form['foundation_extension']['general'] = array(
      '#type' => 'fieldset',
      '#title' => t('General Settings'),
    );
    $form['foundation_extension']['general']['foundation_extension_core_js'] = array(
      '#type' => 'select',
      '#title' => t('Include core JS'),
      '#default_value' => variable_get('foundation_extension_core_js', 'false'),
      '#description' => t('Whether to include the core Foundation 6 JS.'),
      '#options' => array(
        'true' => 'True',
        'false' => 'False'
      ),
    );
    $form['foundation_extension']['general']['foundation_extension_init_foundation'] = array(
      '#type' => 'select',
      '#title' => t('Init Foundation'),
      '#default_value' => variable_get('foundation_extension_init_foundation', 'false'),
      '#description' => t('Whether to include "$(document).foundation();" script in the footer of the page.'),
      '#options' => array(
        'true' => 'True',
        'false' => 'False'
      ),
      '#states' => array(
        'visible' => array(
          'select[name="foundation_extension_core_js"]' => array('value' => 'true')
        ),
      ),
    );
    $form['foundation_extension']['general']['foundation_extension_core_js_version'] = array(
      '#type' => 'select',
      '#title' => t('Core JS version'),
      '#default_value' => variable_get('foundation_extension_core_js_version', 'development'),
      '#description' => t('Whether the JS should be minified.'),
      '#options' => array(
        'development' => 'Development',
        'production' => 'Production'
      ),
      '#states' => array(
        'visible' => array(
          'select[name="foundation_extension_core_js"]' => array('value' => 'true')
        ),
      ),
    );
    $form['foundation_extension']['general']['foundation_extension_core_css'] = array(
      '#type' => 'select',
      '#title' => t('Include core CSS'),
      '#default_value' => variable_get('foundation_extension_core_css', 'false'),
      '#description' => t('Whether to include the core Foundation 6 CSS.'),
      '#options' => array(
        'true' => 'True',
        'false' => 'False'
      ),
    );
    $form['foundation_extension']['general']['foundation_extension_core_css_version'] = array(
      '#type' => 'select',
      '#title' => t('Core CSS version'),
      '#default_value' => variable_get('foundation_extension_core_css_version', 'development'),
      '#description' => t('Whether the CSS should be minified.'),
      '#options' => array(
        'development' => 'Development',
        'production' => 'Production'
      ),
      '#states' => array(
        'visible' => array(
          'select[name="foundation_extension_core_css"]' => array('value' => 'true')
        ),
      ),
    );
    /*
     * Javascript Settings.
     */
    $form['foundation_extension']['js'] = array(
      '#type' => 'fieldset',
      '#title' => t('Javascript Settings'),
    );
    $form['foundation_extension']['js']['markup'] = array(
      '#type' => 'container',
      '#title' => t('Javascript Settings'),
      '#states' => array(
        'visible' => array(
          'select[name="foundation_extension_core_js"]' => array('value' => 'false')
        ),
      ),
    );
    $form['foundation_extension']['js']['markup']['markup'] = array(
      '#markup' => t('Please enable "Include core JS" in the general settings panel to enable these options.'),
    );

    $form['foundation_extension']['js']['foundation_extension_component_set'] = array(
      '#type' => 'select',
      '#title' => t('Core component set'),
      '#default_value' => variable_get('foundation_extension_component_set', 'all'),
      '#description' => t('Whether to include the core Foundation 6 CSS.'),
      '#options' => array(
        'all' => 'All',
        'custom' => 'Custom'
      ),
      '#states' => array(
        'visible' => array(
          array(
            'select[name="foundation_extension_core_js"]' => array('value' => 'true')
          )
        ),
      ),
    );

    # the drupal checkboxes form field definition
    $form['foundation_extension']['js']['foundation_extension_components'] = array(
      '#title' => t('Components'),
      '#type' => 'checkboxes',
      '#default_value' => foundation_extension_components_selected(),
      '#description' => t('Select the Foundation components you would like to load.'),
      '#options' => foundation_extension_components('components'),
      '#prefix' => '',
      '#suffix' => '',
      '#states' => array(
        'visible' => array(
          'select[name="foundation_extension_component_set"]' => array('value' => 'custom'),
          'select[name="foundation_extension_core_js"]' => array('value' => 'true')
        ),
      ),
    );

    /*
     * Javascript Settings.
     */
    $form['foundation_extension']['features'] = array(
      '#type' => 'fieldset',
      '#title' => t('Extra Features'),
    );
    $form['foundation_extension']['features']['foundation_extension_motion_ui'] = array(
      '#type' => 'select',
      '#title' => t('Motion UI'),
      '#default_value' => variable_get('foundation_extension_motion_ui', 'false'),
      '#description' => t('Whether to include the Motion UI library.'),
      '#weight' => 13,
      '#options' => array(
        'true' => 'Enable',
        'false' => 'Disable'
      ),
    );

    $form = system_settings_form($form);
    $form['#submit'][] = 'foundation_extension_admin_form_submit';

    return $form;
  }
}

function foundation_extension_admin_form_submit($form, &$form_state) {
  $js = array();
  $js[] = 'foundation.core.js';
  $component_files = foundation_extension_component_js();
  $components_selected = foundation_extension_components_selected();
  foreach($components_selected as $component) {
    $js[] = $component_files[$component + 1];
  }
  $js = array_merge($js, foundation_extension_util_js());

  $required_js = array();
  $customscript = '';
  $customfiles = variable_get('file_public_path', conf_path() . '/files') . '/foundation';

  if (!is_dir($customfiles)) {
    drupal_mkdir($customfiles);
  }
  foreach($js as $requirement) {
    $content = file_get_contents(drupal_get_path('module', 'foundation_extension') . '/js/core/' . $requirement);
    $customscript .= $content;
  }
  file_put_contents($customfiles . '/foundation.custom.js',$customscript);
}
